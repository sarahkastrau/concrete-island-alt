+++
author =""
title = "Wie es funktioniert"
date = "2020-09-14"
description ="Wie es funktioniert"
tags = ["gebrauchsanweisung"]
draft = false
weight = 20

+++

Jede Austellung findet in einer anderen Stadt statt. Jede Stadt erfordert die Suche nach einem neuen Ort. Diese Erkundungen werden fotografisch dokumentiert.

<!--more-->
Der Ort der Austellung wird über die Website, [Instagram](https://instagram.com/sarahkastrau) und den [Newsletter](http://eepurl.com/gy8uNP) bekannt gegeben. Jede Austellung enthält ein Passwort. Wenn ihr das Passwort gefunden habt geht auf [concrete-island.com/fotobuch](/fotobuch) und gebt das Passwort und eure Anschrift[^1] ein. Der Versand ist konstenlos.

**Stolperfallen**  
Ihr habt die Austellung nicht gefunden? Zu weit? Nasse Socken gekriegt? Das Passwort nicht gefunden? Etwas anderes?  
Macht ein Foto von dem Punkt bis zu dem ihr gekommen seid (GPS-Koordinaten sind auch super) und schickt es mir per [Mail](mailto:contact@concrete-island.com). Ihr erhaltet euer Fotobuch trotzdem.

**Wichtig**  
Das Ganze soll keine Mutprobe sein. Einige Orte sind recht abgelegen, andere schwer zugänglich. Solltet ihr euch aus irgendeinem Grund bei eurer Erkundung unwohl fühlen, dann brecht ab und macht euch auf dem Heimweg. Mir ist es wichtig das bei diesem Projekt der Spaß an der Erkundung des urbanen Raums im Vordergrund steht. Damit möglichlist viele Menschen Spaß an der Sache haben bin ich auf euer Feedback angewiesen. Schreibt mir gerne per Mail oder bei Instagram was ich verbessern kann.


[^1]: Datenschutz: eure Daten werden ausschließlich im Rahmen des Projekts genutzt, nicht veröffentlich oder an andere weitergegeben.